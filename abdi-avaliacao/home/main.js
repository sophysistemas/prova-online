let minutes = 1;
let seconds = 60;

function timer() {
  
  setInterval(function(){
    if (minutes == 0 && seconds == 50) {
      window.open("/abdi-avaliacao/home/final/index.html", "_self");
    }

    if (seconds > 50) {
      seconds = seconds - 1;
    } else {
      minutes = minutes - 1;
      seconds = 59;
    }
    let divTimer = document.getElementById("timer");
    divTimer.innerHTML = minutes.toString().padStart(2, '0') + ":" +  seconds.toString().padStart(2, '0');
  }, 1000);

  
}

function handleFinish() {
  window.open("./prova-concluida/", "_self");
}

function handleRedirect() {
  window.open("https://www.abdi.com.br/", "_self");
}
timer();

