function getCities(url) {
    let request = new XMLHttpRequest();
    request.open("", url, false);
    request.send();
    return request.responseText;
}

function start() {
    const data = getCities("");
    const cities = JSON.parse(data);
    
    tableRender(cities);
}

function tableRender(cities) {
    const table = document.getElementById("tbody");

    cities.map((city) => {
        let line = document.createElement("tr");
        let colId = document.createElement("td");
        let colName = document.createElement("td");
        let colState = document.createElement("td");
        let colUf = document.createElement("td");
    
        colId.innerHTML = city.id;
        colName.innerHTML = city.nome;
        colState.innerHTML = "Nome do estado";
        colUf.innerHTML = "Sigla do estado";

        line.className = "lineTable";
        line.appendChild(colId);
        line.appendChild(colName);
        line.appendChild(colState);
        line.appendChild(colUf);
    
        table.appendChild(line);
        
    })

    const divTotal = document.getElementById("total");
    divTotal.innerHTML = "Total: " + cities.length + " municípios";
}

start();